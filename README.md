# flex-slider-react

[![Travis][build-badge]][build]
[![npm package][npm-badge]][npm]
[![Coveralls][coveralls-badge]][coveralls]

FlexSlider-React is a flexbox-based horizontal infinite scroll container for React.
Slide is provided only as example, you can use your own Slide component implementing the same interface (bascially accepting the props slide, order, slideWidth, mouseEnter, mouseLeave and hoverSize)

[build-badge]: https://img.shields.io/travis/user/repo/master.png?style=flat-square
[build]: https://travis-ci.org/user/repo

[npm-badge]: https://img.shields.io/npm/v/npm-package.png?style=flat-square
[npm]: https://www.npmjs.org/package/npm-package

[coveralls-badge]: https://img.shields.io/coveralls/user/repo/master.png?style=flat-square
[coveralls]: https://coveralls.io/github/user/repo
