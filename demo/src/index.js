import React, {Component} from 'react'
import {render} from 'react-dom'

import { injectGlobal } from 'styled-components';
import FlexSlider from '../../src';
import { Slide } from '../../src';

injectGlobal`
html {
  margin: 0;
  padding: 0;
}

body{
  margin: 0;
  padding: 0;
}
`;


class Demo extends Component {

  render() {
    const slides = [ 
        { bg: 'blue', content: '1'},
        { bg: 'red', content: '2'},
        { bg: 'green', content: '3'},
        { bg: 'yellow', content: '4'},
        { bg: 'orange', content: '5'},
        { bg: 'purple', content: '6'},
        { bg: 'cyan', content: '7'},
        { bg: 'darkblue', content: '8'},
        { bg: 'azure', content: '9'},
        { bg: 'chocolate', content: '10'},
        { bg: 'darkred', content: '11'},
        { bg: 'darkorange', content: '12'},
        { bg: 'lightpink', content: '13'},
        { bg: 'darkcyan', content: '14'},
        { bg: 'darkblue', content: '15'},
        { bg: 'darkyellow', content: '16'},
        { bg: 'darkorange', content: '17'},
        { bg: 'orange', content: '18'},
        { bg: 'blue', content: '19'},
        { bg: 'yellow', content: '20'},
        { bg: 'green', content: '21'},
        { bg: 'darkblue', content: '22'},
    ];

    return (
      <div>
	<span>7 slides, 2.5 visible</span>
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 7) } slidesVisible={ 2.5 } />
      </div>
    );
  }
}
/*
 *	<span>3 slides, 1.3 visible</span>
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 3) } visible={ 1.3 } slideSpeed={ 2 } slideDuration={ 2000 } />
	<span>4 slides, 1.3 visible</span>
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 4) } visible={ 1.3 } />
	<span>7 slides, 1.3 visible</span>
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 7) } visible={ 1.3 } />
 *	<span>8 slides, 5 visible</span>
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 8) } visible={ 5 } />
	<span>14 slides, 7 visible</span>
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 14) } visible={ 7 } />
	<span>18 slides, 9 visible</span>
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 15) } visible={ 9 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 17) } visible={ 7.5 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 18) } visible={ 8.3 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 13) } visible={ 6.9 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 12) } visible={ 9.4 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 16) } visible={ 3.6 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 19) } visible={ 7.4 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 20) } visible={ 2.6 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 21) } visible={ 8.1 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 22) } visible={ 4.2 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 22) } visible={ 5.5 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 10) } visible={ 6.4 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 11) } visible={ 8.2 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 12) } visible={ 5.2 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 13) } visible={ 4.5 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 14) } visible={ 9.3 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 15) } visible={ 2.5 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 16) } visible={ 3.5 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 17) } visible={ 5.2 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 18) } visible={ 9.2 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 19) } visible={ 5.6 } />
        <FlexSlider slideComponent={ Slide } slides={ slides.slice(0, 22) } visible={ 4.3 } />
	*/

render(<Demo/>, document.querySelector('#demo'))
