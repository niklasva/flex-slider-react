import React, { Component } from 'react';
//import Slide from './Slide';
import styled from 'styled-components';

const SliderWrapper = styled.div`
  position: relative;

  /* FIXME: This is to prevent horizontal scrollbar, the slider is actually a bit too large, this should probably be fixed.*/
  overflow-x: hidden;
`;

const ScrollContainer = styled.div`
  width: 100vw;
  display: flex;
  overflow-x: scroll;
  scroll-snap-type: x proximity;
  /* hide the scrollbar */
  margin-bottom: -${ props => props.scrollHeight }px;
`;

const Slider = styled.div`
  /*width: ${props => { const count = React.Children.count(props.children);
                      return (100*count)/props.numVisible}}vw;*/
  display: flex;
  flex-grow: 0;
  flex-shrink: 1;
  align-items: center;
  justify-content: stretch;
  flex-direction: row;
  flex-wrap: nowrap;
`;

const DirectionButton = styled.div`
  height: 100%;
  width: 4em;
  background-color: rgba(255,255,255, 0.75);
  position: absolute;
  z-index: 100;
  top: 0;
  ${ props => props.direction }: 0;
`;

//TODO: The body gets a weird x-scroll. This could probably be fixed by setting
// overflow-x hidden, but that just seems like an ugly solution.

class FlexSlider extends Component {

  constructor(props) {
    super(props);
    this.state = {
      /* This is the position of the slider, counted in number of slides from
       * the left. We set this to a large number inititally in order to avoid
       * negative numbers It is used in order to calculcate the flex-order of 
       * the slides, simulating an "infintie" slider */
      position: 1000000,
      /* This is an array representing the contents of each slide, to be
       * interpreted by the custom Slide component. It is first passed as a
       * prop, however in order to guarantee a sufficient number of slides we
       * use the incoming prop and duplicate it until a minimum has been reached */
      slides: [],
    };
    
    /* Reference to the scroll container,
     * used to read and manipulate the state of the scroll */
    this.scroll = null;
    // This function is used to set the reference in lieu of React.createRef()
    this.setScrollRef = element => {
      this.scroll = element;
    }

    // Bind class functions to the current object
    this.animateScroll = this.animateScroll.bind(this);
    this.handleSlideMouseEnter = this.handleSlideMouseEnter.bind(this);
    this.handleSlideMouseLeave = this.handleSlideMouseLeave.bind(this);
  }

  /* Given the props, comput a new state. This is a React 16 feature,
   * but it is called from componentWillMount in React 15. */
  static getDerivedStateFromProps(nextProps, prevState) {
    let derivedState = {
      // If a slide speed is not provided, set it to number of visible slides-1
      slideSpeed: nextProps.slideSpeed ? 
	    nextProps.slideSpeed : Math.floor(nextProps.slidesVisible-1) || 1,
      slides: null,
      // The duration of a click-induced sliding-anmiation
      slideDuration: nextProps.slideDuration ? nextProps.slideDuration : 200,
    }

    // concat nextProps.slides to prevState.slides until
    // it has at least minMult*slideSpeed slides
    const minMult = 2.5; 
    // Absolute minimum number of slides
    const minSlides = 4;

    if( nextProps.slides.length > minMult*derivedState.slideSpeed &&
		nextProps.slides.length > minSlides) {
      derivedState.slides = nextProps.slides;
    } else {
      let slides = this.state.slides;
      while(slides.length <= minMult*derivedState.slideSpeed || 
		slides.length <= minSlides) {
        slides = slides.concat(nextProps.slides);
      }
      derivedState.slides = slides;
    }
    return derivedState;
  }
  
  componentWillMount() {
    // React < 16.3 compatibility
    const version = React.version.split(".");
    if(!(version[0] >= 16 && version[1] >= 3)) {
      this.setState(this.constructor.getDerivedStateFromProps(this.props, this.state));
    }
  }

  componentDidMount() {
    // Get the totalt of the scrollable area (scrollWidth) 
    // and the width of the visible area (innerWidth)
    this.scrollWidth = this.scroll.scrollWidth;
    this.innerWidth = this.scroll.clientWidth;

    // Calculate the width of slides
    this.slideCount = this.state.slides.length;
    this.slideWidth = this.scrollWidth/this.slideCount;

    // Midpoint is different depending on odd or even number of slides
    if(this.props.slidesVisible > 2) {
      this.midPoint = Math.floor(this.scrollWidth/2-this.innerWidth/2 
			  - (this.slideCount%2===0 ? 0 : this.slideWidth/2));
    } else {
      this.midPoint = Math.floor(this.scrollWidth/2-this.innerWidth/2
                          - (this.slideCount%2===0 ? this.slideWidth/2 : 0));
    }
    // Center the scroll
    this.scroll.scrollLeft = this.midPoint;
    this.scroll.addEventListener('scroll', () => this.handleScroll());

    const scrollBarHeight = this.scroll.offsetHeight - this.scroll.clientHeight;
    if(scrollBarHeight != 15) {
	// The most common height is 15px, and setting the state will trigger a rerender, 
        // so instead we will simply set the default value to 15 and only set the state
	// and trigger a rerender in case it's something other than 15.
        this.setState({ scrollBarHeight: 15 });
    }
  }

  componentWillUnmount() {
    this.scroll.removeEventListener('scroll', () => this.handleScroll());
  }

  /* This function is called by scroll listener every time scroll position changes.
   * It checks the distance from the "midPoint" (calculated in componentDidMount)
   * and if it's larger than a specified width, it will reorder the slides such that
   * the last slide becomes the first slide, the second becomes the third, and so forth.
   * it then resets the scroll to the "midPoint" and adds the remainder between the
   * total distance covered by the reordered slides and the scroll distance to
   * the midPoint. The result is a smooth infinite scroll.
   */
  handleScroll() {
    if(this.animating) {
      // If animating, don't do anything until the animation has finished
      return;
    }
    const scrollDiff = this.scroll.scrollLeft - this.midPoint;
    console.log(scrollDiff);

    if(Math.abs(scrollDiff) >= this.slideWidth-20) {
      const posDiff = Math.ceil(scrollDiff/this.slideWidth);
      // The remainder, we have to adjust the scroll position properly using this
      const remainder = scrollDiff - posDiff*this.slideWidth;
      let newPos = 0;
      if(scrollDiff > 0) {
        newPos = this.state.position - posDiff;
      } else {
        newPos = this.state.position - posDiff;
      }

      // Change position so that the order of the elements will shift posDiff places in a ring
      this.setState({
	      position: newPos,
      });

      // Scroll to correct location so it doesn't appear like anything has been done
      this.scroll.scrollLeft = this.midPoint + remainder;
      // So that any active animation will adjust its target
      this.animationTarget -= scrollDiff;
      console.log('new position: ' + newPos);
    }
  }

  moveSliderRight = () => {
    const newPos = this.midPoint + 
		  this.state.slideSpeed*this.slideWidth;
    this.smoothScroll(newPos, 1);
    // In case the last change is too small for the scroll listener to be triggered,
    // trigger handleScroll manually here.
    this.handleScroll();
  }

  moveSliderLeft = () => {
    const newPos = this.midPoint - 
		  this.state.slideSpeed*this.slideWidth
    this.smoothScroll(newPos, -1);
    // In case the last change is too small for the scroll listener to be triggered,
    // trigger handleScroll manually here.
    this.handleScroll();
  }

  /* Animate scoll to specified location and in the specified direction */
  smoothScroll = (scrollLeft, direction) => {
    if(this.animating) {
      // wait for the animation to finish before allowing another animation
      return;
    }

    // Initialize some parameters which are used by animateScroll
    this.animationStart = this.midPoint;
    this.animationTarget = scrollLeft;
    this.animationDirection = direction;
    this.animationDuration = this.state.slideDuration;
    this.animationStartTimestamp = null;
    this.animating = true;

    // Note the use of requestAnmiationFrame, this is a big boon for performance
    window.requestAnimationFrame(this.animateScroll);
  }

  /* This is the function doing the actual slide animation.
   * It operates on a recursvie principle and calculates the
   * current position given the timestamp provided by requestAnimationFrame
   * and the parameters initialized in smoothScroll */
  animateScroll = (timestamp) => {
    // In the first frame, set the start-time of the animation
    if(!this.animationStartTimestamp) {
      this.animationStartTimestamp = timestamp;
    }
    // Calculate the time passed since the animation was started
    const timePassed = timestamp - this.animationStartTimestamp;
    const scrollLeft = this.scroll.scrollLeft;
    // Calculate the new position given the time passed, where the animation
    // was started, the desired duration of a slide-animation and the desired target position
    const newScrollLeft = this.animationStart + 
		  (timePassed/this.animationDuration)*(this.animationTarget-this.animationStart);
    // Check wether the desired target has been reached
    const targetReached = this.animationDirection*newScrollLeft > 
				  this.animationDirection*this.animationTarget;
    if(!targetReached && timePassed < this.animationDuration) {
      // Set the new position of the scrollbar.
      this.scroll.scrollLeft = newScrollLeft;
      // Request a new frame from this same function
      window.requestAnimationFrame(this.animateScroll);
    } else {
      this.animating = false;
      // Fine-tune the final position of the scroll. In most cases this will be a
      // tiny adjustment compared to newScrollLeft.
      this.scroll.scrollLeft = this.animationTarget;
    }
  }

  render() {
    const { slidesVisible, onSlideClick, selectedSlide } = this.props;
    const { position, slides } = this.state;

    const Slide = this.props.slideComponent;

    let listOfSlides = [];
    for(let idx = 0; idx < slides.length; idx++) {
      const slide = slides[idx];
      const order = ((idx+position) % slides.length);
      listOfSlides.push(
        <Slide 
          key={idx} 
          slide={ slide } 
          order={ order } 
          slideWidth={ 100/slidesVisible } 
          sliderWidth={ this.sliderWidth }
	  onSlideClick={ onSlideClick }
	  onSlideEnter={ onSlideEnter } />
      )
    }
    const scrollBarHeight = this.state.scrollBarHeight ? this.state.scrollBarHeight : 15;

    return (
      <SliderWrapper>
	<DirectionButton direction={ 'left' } onClick={ () => this.moveSliderLeft() } scrollHeight={ scrollBarHeight } />
	<ScrollContainer innerRef={ this.setScrollRef } scrollHeight={ scrollBarHeight }>
          <Slider numVisible={ slidesVisible }>
	      { listOfSlides }
          </Slider>
        </ScrollContainer>
	<DirectionButton direction={ 'right' }  onClick={ () => this.moveSliderRight() } scrollHeight={ scrollBarHeight } />
      </SliderWrapper>
    );
  }
}

export default FlexSlider;
