import React, { Component } from 'react';
import styled from 'styled-components';

const SlideDiv = styled.div`
  // The width property really only controls the width of the slide relative 
  // to other slides, not the absolute width of the slide. 
  // The width of the slide is controlled by the visible width of the scroll 
  // container and the number of visible slides.
  width: ${ props => props.slideWidth }vw;
  flex-shrink: 1;

  // props.slideWidth is 100/numVisible, and 1/0.5625 === 16:9
  height: ${ props => 0.5625*props.slideWidth }vw;

  // Box sizing includes borders, margins, and padding
  //box-sizing: border-box;

  // This centers the contents of the slide
  display: flex;
  justify-content: center;
  align-items: center;

//  background-image: url(https://source.unsplash.com/random/400x300);
//  background-repeat: no-repeat;
//  background-size: cover;

  @media only screen and (min-width: 64em) {
    &:hover {
      // Make the slide hoverSize larger than other slides on hover
      width: ${ props => props.hoverSize*props.slideWidth }vw;
      flex-shrink: ${ props=> 1/props.hoverSize }vw;
      height: ${ props => 0.5625*props.hoverSize*props.slideWidth }vw;
    }
  }
`;

const Content = styled.span`
  font-size: 40pt;
  font-family: 'Arial';
  font-weight: 'bold';
`;

class Slide extends Component {
  render() {
    const { slide, order, slideWidth, mouseEnter, mouseLeave, hoverSize } = this.props;
    // Note that order and backgroundColor change very frequently, generating tons of classes
    // to reflect this may hurt performance, thus they are set as simple style property.
    return (<SlideDiv onMouseEnter={ mouseEnter } onMouseLeave={ mouseLeave } slideWidth={ slideWidth } hoverSize={ hoverSize } style={{ order: order, backgroundColor: slide.bg }}>
              <Content>{ slide.content }</Content>
            </SlideDiv>)
  }
}

export default Slide;
