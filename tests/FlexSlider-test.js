import expect from 'expect'
import React from 'react'
import {render, unmountComponentAtNode} from 'react-dom'

import FlexSlider from 'src/'
import { Slide } from 'src/'

describe('FlexSlider', () => {
  let node

  beforeEach(() => {
    node = document.createElement('div')
  })

  afterEach(() => {
    unmountComponentAtNode(node)
  })

  it('No test', () => {
	  /*
    const slides = [
        { bg: 'blue', content: '1'},
        { bg: 'red', content: '2'},
        { bg: 'green', content: '3'},
        { bg: 'yellow', content: '4'},
    ]
    render(<FlexSlider slideComponent={ Slide } slides={ slides } visible={ 2 } />, node, () => {
    })
    */
  })
})
